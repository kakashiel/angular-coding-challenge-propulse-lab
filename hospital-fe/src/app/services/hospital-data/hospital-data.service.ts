import { Injectable } from '@angular/core';
import { PatientsRegister } from 'hospital-lib';

@Injectable({
  providedIn: 'root'
})
export class HospitalDataService {
  constructor() { }
  parsePatientRequest(input: string): PatientsRegister {
    const result: PatientsRegister = {};
    const characters = input.split(',');
    characters.forEach(char => {
      const trimmedChar = char.trim();

      if (result.hasOwnProperty(trimmedChar)) {
        result[trimmedChar]++;
      } else {
        result[trimmedChar] = 1;
      }
    });
    return result;
  }

  parseDrugRequest(input: string): string[] {
    return input.split(',');
  }

}
