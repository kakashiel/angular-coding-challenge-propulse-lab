import { TestBed } from '@angular/core/testing';

import { HospitalDataService } from './hospital-data.service';

describe('HospitalDataService', () => {
  let service: HospitalDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HospitalDataService);
  });

  it('should parse patients', () => {
    expect(service.parsePatientRequest('h, e, l, l, o')).toEqual({
      h: 1,
      e: 1,
      l: 2,
      o: 1
    });
  });

  it('should parse drugs', () => {
    expect(service.parseDrugRequest('al,be,ce')).toEqual(['al', 'be', 'ce']);
  });
});
