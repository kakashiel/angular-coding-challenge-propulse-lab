import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HospitalRequestService } from './hospital-request.service';


describe('hospital-request service', () => {
  let service: HospitalRequestService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HospitalRequestService]
    });

    service = TestBed.inject(HospitalRequestService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get drugs', () => {
    const dummyData = "al, be, ce";

    service.getDrugs().subscribe(data => {
      expect(data).toEqual(dummyData);
    });

    const req = httpMock.expectOne(`${service.apiUrl}/drugs`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyData);

    httpMock.verify();
  });

});