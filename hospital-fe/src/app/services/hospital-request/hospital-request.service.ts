import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HospitalRequestService {
  public apiUrl = 'http://localhost:7200';

  constructor(private http: HttpClient) {}

  getPatients(): Observable<string> {
    return this.http.get<string>(`${this.apiUrl}/patients`);
  }
  getDrugs(): Observable<string> {
    return this.http.get<string>(`${this.apiUrl}/drugs`);
  }
}
