import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ButtonComponent } from './atoms/button/button.component';
import { HospitalTableComponent } from './organismes/hospital-table/hospital-table.component';
import { HomeComponent } from './pages/home/home.component';
import { HospitalRequestService } from './services/hospital-request/hospital-request.service';


@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
    ])
  ],
  providers: [HospitalRequestService],
  declarations: [
    AppComponent,
    HomeComponent,
    HospitalTableComponent,
    ButtonComponent
  ],
  exports: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }