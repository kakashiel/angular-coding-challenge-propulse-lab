import { Component } from '@angular/core';
import { PatientsRegister, Quarantine } from 'hospital-lib';
import { forkJoin } from 'rxjs';
import { HospitalTableItems } from '../../organismes/hospital-table/hospital-table.component';
import { HospitalDataService } from '../../services/hospital-data/hospital-data.service';
import { HospitalRequestService } from '../../services/hospital-request/hospital-request.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})

export class HomeComponent {
  patients: PatientsRegister = {};
  drugs: string[] = [];
  lastCalculateFinalIndex: number = 0;
  lastFetchPatientlIndex: number = 0;
  items: HospitalTableItems[] = [];

  constructor(
    private hospitalRequestService: HospitalRequestService,
    private hospitalDataService: HospitalDataService
  ) { }

  onFetchDataClick(): void {
    forkJoin([
      this.hospitalRequestService.getPatients(),
      this.hospitalRequestService.getDrugs()
    ]).subscribe(([patientsResult, drugsResult]) => {
      this.patients = this.hospitalDataService.parsePatientRequest(patientsResult);
      this.drugs = this.hospitalDataService.parseDrugRequest(drugsResult);

      const item: HospitalTableItems = {
        id: this.lastFetchPatientlIndex + 1,
        drugs: this.drugs,
        initialStates: this.patients,
        finalStates: undefined
      };
      this.items = [...this.items, item];
      if (this.items.length > 5)
        this.items = this.items.slice(1, 6);
      this.lastFetchPatientlIndex++;

    });
  }
  onCalculateDataClick(): void {
    const lastCalculateFinalIndex = this.items.findIndex(item => item.finalStates === undefined);

    if (lastCalculateFinalIndex < 0 || lastCalculateFinalIndex >= this.items.length) {
      return;
    }
    const item = this.items[lastCalculateFinalIndex];
    const quarantine: Quarantine = new Quarantine(item.initialStates);
    quarantine.setDrugs(item.drugs);
    quarantine.wait40Days();
    const report = quarantine.report();
    console.log(report);
    item.finalStates = report;
    
    this.items[lastCalculateFinalIndex] = item;
  }
}