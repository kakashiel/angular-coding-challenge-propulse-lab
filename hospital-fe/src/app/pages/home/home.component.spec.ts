import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { PatientsRegister } from 'hospital-lib';
import { of } from 'rxjs';
import { ButtonComponent } from '../../atoms/button/button.component';
import { HospitalTableComponent, HospitalTableItems } from '../../organismes/hospital-table/hospital-table.component';
import { HospitalDataService } from '../../services/hospital-data/hospital-data.service';
import { HospitalRequestService } from '../../services/hospital-request/hospital-request.service';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockHospitalRequestService: jasmine.SpyObj<HospitalRequestService>;
  let mockHospitalDataService: jasmine.SpyObj<HospitalDataService>;

  beforeEach(() => {
    mockHospitalRequestService = jasmine.createSpyObj('HospitalRequestService', ['getPatients', 'getDrugs']);
    mockHospitalDataService = jasmine.createSpyObj('HospitalDataService', ['parsePatientRequest', 'parseDrugRequest']);

    TestBed.configureTestingModule({
      declarations: [HomeComponent, ButtonComponent, HospitalTableComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: HospitalRequestService, useValue: mockHospitalRequestService },
        { provide: HospitalDataService, useValue: mockHospitalDataService },
      ],
    });

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch data and add item to items array', fakeAsync(() => {
    const patients: PatientsRegister = {};
    const drugs: string[] = []; 

    mockHospitalRequestService.getPatients.and.returnValue(of());
    mockHospitalRequestService.getDrugs.and.returnValue(of());
    mockHospitalDataService.parsePatientRequest.and.returnValue(patients);
    mockHospitalDataService.parseDrugRequest.and.returnValue(drugs);

    component.onFetchDataClick();
    tick();

    expect(mockHospitalRequestService.getPatients).toHaveBeenCalled();
    expect(mockHospitalRequestService.getDrugs).toHaveBeenCalled();

    expect(component.items.length).toBe(0);
  }));

  it('should calculate data and update finalStates in items array', () => {
    const mockItem: HospitalTableItems = {
      id: 1,
      drugs: ['Drug1', 'Drug2'],
      initialStates: { X: 1 },
      finalStates: undefined,
    };
    component.items = [mockItem];

    component.onCalculateDataClick();

    expect(component.items[0].finalStates).toBeDefined();
  });
});