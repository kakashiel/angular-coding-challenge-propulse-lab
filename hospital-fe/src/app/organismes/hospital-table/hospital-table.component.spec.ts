import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HospitalTableComponent } from './hospital-table.component';
import { HospitalRequestService } from '../../services/hospital-request/hospital-request.service';

describe('HospitalTableComponent', () => {
  let component: HospitalTableComponent;
  let fixture: ComponentFixture<HospitalTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HospitalTableComponent],
      imports: [HttpClientTestingModule],
      providers: [HospitalRequestService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HospitalTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display data in the table', () => {
    const testData = [
      { id: 1, drugs: ['drug 1'], initialStates: {a: 1}, finalStates: {a: 2} },
      { id: 2, drugs: ['drug 2'], initialStates: {a: 2}, finalStates: {a: 3} },
    ];

    component.hospitalTableItems = testData;
    fixture.detectChanges();

    const tableRows = fixture.nativeElement.querySelectorAll('tbody tr');
    expect(tableRows.length).toEqual(testData.length);

    tableRows.forEach((row, index) => {
      const columns = row.querySelectorAll('td');
      expect(columns[0].textContent).toContain(testData[index].id);
      expect(columns[1].textContent).toContain(testData[index].drugs);
      expect(columns[2].textContent).toContain(JSON.stringify(testData[index].initialStates, null, 4));
      expect(columns[3].textContent).toContain(JSON.stringify(testData[index].finalStates, null, 4));
    });
  });
});
