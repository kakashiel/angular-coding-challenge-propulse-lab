import { Component, Input } from '@angular/core';
import { PatientsRegister } from 'hospital-lib';

export interface HospitalTableItems {
  id: number;
  drugs: string[];
  initialStates: PatientsRegister;
  finalStates?: PatientsRegister;
}
@Component({
  selector: 'app-hospital-table',
  templateUrl: './hospital-table.component.html',
  styleUrl: './hospital-table.component.scss'
})

export class HospitalTableComponent {
  @Input() hospitalTableItems: HospitalTableItems[] = [];
  printPatients(patients?: PatientsRegister) {
    return JSON.stringify(patients, null, 4);
  }
}