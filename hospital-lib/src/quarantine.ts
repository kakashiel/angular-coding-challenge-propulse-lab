import { PatientsRegister } from './patientsRegister';

export class Quarantine {

    private nbStartPatientFever = 0;
    private nbStartPatientHealty = 0;
    private nbStartPatientDiabetes = 0;
    private nbStartPatientTuberculosis = 0;
    private nbStartPatientDead = 0;

    private nbEndPatientFever = 0;
    private nbEndPatientHealty = 0;
    private nbEndPatientDiabetes = 0;
    private nbEndPatientTuberculosis = 0;
    private nbEndPatientDead = 0;

    private isDrugAspirin = false;
    private isDrugAntibiotic = false;
    private isDrugInsulin = false;
    private isDrugParacetamol = false;


    constructor(patients: PatientsRegister) {
        this.nbStartPatientFever = patients.F || 0;
        this.nbStartPatientHealty = patients.H || 0;
        this.nbStartPatientDiabetes = patients.D || 0;
        this.nbStartPatientTuberculosis = patients.T || 0;
        this.nbStartPatientDead = patients.X || 0;

        this.nbEndPatientDiabetes = this.nbStartPatientDiabetes;
        this.nbEndPatientHealty = this.nbStartPatientHealty;
        this.nbEndPatientTuberculosis = this.nbStartPatientTuberculosis;
        this.nbEndPatientDead = this.nbStartPatientDead;
        this.nbEndPatientFever = this.nbStartPatientFever;
    }

    public setDrugs(drugs: Array<string>): void {
        drugs.forEach(drug => {
            if (drug === 'As') {
                this.isDrugAspirin = true;
            } else if (drug === 'An') {
                this.isDrugAntibiotic = true;
            } else if (drug === 'I') {
                this.isDrugInsulin = true;
            } else if (drug === 'P') {
                this.isDrugParacetamol = true;
            }
        })
    }

    public wait40Days(): void {
        if (this.isDrugAspirin) {
            this.giveAspirin();
        }
        if (this.isDrugAntibiotic) {
            this.giveAntibiotic();
        }
        if (!this.isDrugInsulin) {
            this.dontGiveInsulin();
        }
        if (this.isDrugInsulin && this.isDrugAntibiotic) {
            this.giveInsulinAndAntibiotic();
        }
        if (this.isDrugParacetamol) {
            this.giveParacetamol();
        }
        if (this.isDrugAspirin && this.isDrugParacetamol) {
            this.giveAspirinAndParacetamol();
        }

    }

    private giveParacetamol() {
        this.nbEndPatientHealty = this.nbStartPatientHealty + this.nbStartPatientFever;
        this.nbEndPatientFever = 0;
    }

    private giveInsulinAndAntibiotic() {
        this.nbEndPatientFever += this.nbStartPatientHealty;
        this.nbEndPatientHealty -= this.nbStartPatientHealty;
    }

    private dontGiveInsulin() {
        this.nbEndPatientDead += this.nbStartPatientDiabetes;
        this.nbEndPatientDiabetes = 0;
    }

    private giveAntibiotic() {
        this.nbEndPatientHealty = this.nbStartPatientHealty + this.nbStartPatientTuberculosis;
        this.nbEndPatientTuberculosis = 0;
    }

    private giveAspirin() {
        this.nbEndPatientHealty = this.nbStartPatientHealty + this.nbStartPatientFever;
        this.nbEndPatientFever = 0;
    }

    private giveAspirinAndParacetamol() {
        this.nbEndPatientDead = 
            this.nbStartPatientDead 
            + this.nbStartPatientHealty
            + this.nbStartPatientDiabetes
            + this.nbStartPatientFever
            + this.nbStartPatientTuberculosis;
        this.nbEndPatientHealty = 0;
        this.nbEndPatientTuberculosis = 0;
        this.nbEndPatientDiabetes = 0;
        this.nbEndPatientFever = 0;
    }

    public report(): PatientsRegister {
        return {
            F: this.nbEndPatientFever,
            H: this.nbEndPatientHealty,
            D: this.nbEndPatientDiabetes,
            T: this.nbEndPatientTuberculosis,
            X: this.nbEndPatientDead
        };
    }
}
