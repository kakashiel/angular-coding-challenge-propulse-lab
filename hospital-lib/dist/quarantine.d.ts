import { PatientsRegister } from './patientsRegister';
export declare class Quarantine {
    private nbStartPatientFever;
    private nbStartPatientHealty;
    private nbStartPatientDiabetes;
    private nbStartPatientTuberculosis;
    private nbStartPatientDead;
    private nbEndPatientFever;
    private nbEndPatientHealty;
    private nbEndPatientDiabetes;
    private nbEndPatientTuberculosis;
    private nbEndPatientDead;
    private isDrugAspirin;
    private isDrugAntibiotic;
    private isDrugInsulin;
    private isDrugParacetamol;
    constructor(patients: PatientsRegister);
    setDrugs(drugs: Array<string>): void;
    wait40Days(): void;
    private giveParacetamol;
    private giveInsulinAndAntibiotic;
    private dontGiveInsulin;
    private giveAntibiotic;
    private giveAspirin;
    private giveAspirinAndParacetamol;
    report(): PatientsRegister;
}
